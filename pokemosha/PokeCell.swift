//
//  PokeCell.swift
//  pokemosha
//
//  Created by Алем Утемисов on 09.03.16.
//  Copyright © 2016 Алем Утемисов. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var thumbImg : UIImageView!
    
    var pokemon : Pokemon!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 15
    }
    
    func configureCell(pokemon: Pokemon){
        self.pokemon = pokemon
        nameLbl.text = self.pokemon.name.capitalizedString
        thumbImg.image = UIImage(named: "\(self.pokemon.pokedexId)")
    }
}
